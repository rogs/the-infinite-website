#+TITLE: The Infinite Website

The Infinite Website is a web application that dynamically generates HTML on runtime using ChatGPT and
serves it in an async Flask application.

Of course, this README was also made with ChatGPT.

You can visit the site here: https://theinfinitewebsite.com. Be aware this is VERY slow, since ChatGPT
has to generate the HTML in runtime. Take this like a "1999 internet emulator".

#+ATTR_HTML: :alt logo :title logo :align left
[[https://gitlab.com/rogs/the-infinite-website/-/raw/master/image.png]]

/This image was generated with Dall-E. The propmt was/
#+begin_quote
An infinite, breathtaking, stunning space landscape. Focus on the "infinite" part.
#+end_quote

* Features
:PROPERTIES:
:ID:       7246700b-5468-4987-8f64-49802281034c
:END:

  - Dynamic HTML generation using ChatGPT
  - Async Flask application
  - Interactive and engaging web experiences

* Prerequisites
:PROPERTIES:
:ID:       824e7cc7-8e7f-4bd5-925f-04705c91b130
:END:

  - Python 3.6 or higher
  - pip package manager

* Installation
:PROPERTIES:
:ID:       05b421fe-de3b-4971-bf23-f22f2ae1e696
:END:

  1. Clone the repository:
     #+BEGIN_SRC shell
     git clone https://gitlab.com/rogs/the-infinite-website.git
     #+END_SRC

  2. Navigate to the project directory:
     #+BEGIN_SRC shell
     cd the-infinite-website
     #+END_SRC

  3. Create a ~virtualenv~ and install the required Python dependencies:
     #+BEGIN_SRC shell
     virtualenv .venv
     source .venv/bin/activate
     (.venv) pip install -r requirements.txt
     #+END_SRC

  4. Create your .env file and add your ~OPENAI_KEY~:
     #+BEGIN_SRC shell
     cp .env.example .env

     # Edit the .env file and add your OPENAI_KEY
     #+END_SRC

* Usage
:PROPERTIES:
:ID:       06058750-d1ae-4ab1-a539-b7e02f342616
:END:

  1. With the ~virtualenv~ activated, start the Flask server:
     #+BEGIN_SRC shell
     (.venv) flask run
     #+END_SRC

  2. Access the application by visiting http://localhost:5000 in your web browser.

* Performance Considerations
:PROPERTIES:
:ID:       21e9db08-84d3-4b0d-97a7-8a9baca6903d
:END:

  The dynamic HTML generation process performed by ChatGPT during runtime can introduce some performance
  overhead, especially for complex or computationally intensive requests. As a result, the app's response
  times may be slower compared to pre-rendered static HTML websites.

* Contributing
:PROPERTIES:
:ID:       95d869f4-35ce-49ff-b8bd-427eafe4bd01
:END:

  Contributions are welcome! If you would like to contribute to The Infinite Website, please follow these
  steps:

  1. Fork the repository.
  2. Create a new branch: ~git checkout -b feature/your-feature-name~.
  3. Make your changes and commit them: ~git commit -am "Add some feature"~.
  4. Push the branch to your forked repository: ~git push origin feature/your-feature-name~.
  5. Open a merge request on GitLab.

* License
:PROPERTIES:
:ID:       187fd999-2ae8-4d35-ad65-b65d1c8f9612
:END:

  [[file:LICENSE][MIT License]]

* Acknowledgements
:PROPERTIES:
:ID:       0c62b0f4-be51-49f4-be11-ef7c8357582a
:END:

  - This project was inspired by the concept of generating dynamic HTML using ChatGPT and Flask.
  - Special thanks to the contributors and open source community for their valuable contributions and
    support.

* Contact
:PROPERTIES:
:ID:       a4fc73ae-9bac-4184-abd7-be4cc824ad2f
:END:

  For any questions or inquiries, please reach out to the project maintainer:

  - Roger Gonzalez
  - Email: roger at rogs.me
  - GitLab: [[https://gitlab.com/rogs][@rogs]]
