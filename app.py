import logging
import os

import openai
from dotenv import load_dotenv
from flask import Flask, redirect, request

from chatgpt_helper import get_chatgpt_html_response
from htmls import index_html

load_dotenv()

app = Flask(__name__)
openai.api_key = os.getenv("OPENAI_KEY")
logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")


@app.route("/")
def index():
    return index_html


@app.route("/start")
async def start():
    return await get_chatgpt_html_response()


@app.route("/infinite")
async def topic():
    topic = request.args.get("topic", None) or request.args.get("topics", None)
    if not topic:
        return redirect("/start")

    unslugified_topic = topic.replace("-", " ").replace("_", " ").replace("%20", " ").capitalize()

    if len(unslugified_topic.split()) > 5:
        logging.info(f"The topic is too long: {unslugified_topic}")
        return "Error! Your topic should be < 5 words"

    return await get_chatgpt_html_response(unslugified_topic)
